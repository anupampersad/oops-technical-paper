 # Object Oriented Programming 
 
 Languages can be divided as

 1. Procedure Oriented Programming languages like C 
 2. Object Oriented Programming Languages like Java, Python, JavaScript.

## What is OOP 

<p>OOP (short for <em>Object Oriented Programming</em>) is a programming methodology in which we use objects and classes instead of normal functions and procedures.</p>

<p>A <strong>class</strong> is a module that itself contains data and methods (also called functions) to achieve a specific task. A class can also be defined as a blueprint from which you can create an individual object. <strong>Objects</strong> can be thought of as an instance of a class.</p>

<p>The main task in OOP methodology is to divide the problem at hand into several sub-tasks and these are called classes. Here each class can perform several inter-related tasks for which several methods can be implemented in an individual class.</p>


## Let us understand OOP with an analogy – 

	
- ### Analogy 1 

We have a group of humans, animals like cats and dogs and birds.

Now we can observe that all humans have similar properties like they can talk, have two arms and two legs. Similarly, animals have 4 legs and all birds can fly and have a pair of wings.

Each group has a set of similar behavior and such objects with similar behavior belong to the same class. *So, a class represents a common behavior of a group of objects.*<br>
Hence, we can say Human is a class and can have instances/objects like Bruce, Shubham, Diana. Similarly, Bird is a class, and Crow, Eagle are the objects of bird class.<br>
*Every object has some behavior. The behavior of an object is represented by attributes and actions.*

For example – 

Bruce is an object of class Human and has attributes/properties like name, age, gender, etc. Attributes can be represented by variables in programming.
Also, Anupam can perform actions like walking, talking which can be termed as methods or functions of the given object.

- ### Analogy 2


We have a collection of different shapes like squares, rectangles, triangles, circles. Now we can observe that all squares have similar properties like all sides are equal. Similarly, all the rectangles have opposite sides equal.

A square can have attributes such as the length of the side, the color of the square. 
Also, the square can have functions or methods such as calculatePerimeter , calculateArea.
<br>
<br>


# Features of Object Oriented Programming


Object Oriented Programming has four important features namely :
- Inheritance
- Encapsulation
- Polymorphism
- Abstraction 


## 1) Encapsulation
Encapsulation is a method in which data and methods/functions are bundled together in a single unit.
Encapsulation is also known as data hiding.
<br>
<br>

## 2) Inheritance
Inheritance is a process in which a child class acquires data and methods from a parent class (also called superclass). By inheritance, new classes can acquire all the features of existing classes and can add to them.
<br>
<br>

## 3) Polymorphism
The word Polymorphism came from two Greek words 'Poly' meaning 'many' and 'morphos' meaning 'forms'. Thus polymorphism represents the ability to assume several different forms.
Whenever an object exhibits different behavior in different contexts, it is called polymorphic nature.
<br>
<br>

## 4) Abstraction
Abstraction is a process in which we only expose the functionality to the user and hide all the implementation details from the user. Due to abstraction, the user will know only what a code does and not how it is implemented. 
<br>
<br>

*We will see the implementation of the above concepts in code in the following sections.*

<br>

# OOP using JavaScript

### <u>Creating a class in JavaScript</U>
Whenever an instance of a class is created in JavaScript, its **constructor** function is called.

In class-based object-oriented programming, a constructor is a special type of subroutine called to create an object. It prepares the new object for use, often accepting arguments that the constructor uses to set required member variables.
```javascript
class Rectangle {

    constructor(_width,_height,_color){ 

        console.log('Rectangle is being created')
        this.width = _width
        this.height = _height
        this.color = _color
    }

    getArea = function(){
        return this.height * this.width
    }

    getPerimeter = function(){
        return 2 * this.width * this.height
    }

```
In the above code - `width, height, color `are the *properties/attributes* of the object while `getArea()`, `getPerimeter()` are the *methods/functions* of the object.
<br>
<br>


### <u>Creating Instance of a class in JavaScript</U>
For the above class, we can create an Object as follows :

```javascript
let rect1 = new Rectangle(5,10,'red');
rect1.getArea();
rect1.getPerimeter();

```
>Output
```javascript
Rectangle is being created
50
100
```

We pass arguments when creating the object **rect1** of the **rectangle class**.
After creating the object, we can call methods `getArea()` and `getPerimeter()`.
<br>
<br>

# <u>OOP Features implementation in JavaScript.</u>


## 1) Encapsulation

```javascript
class Square {

    #side;

    constructor(){
    }

    setSide = function(side){
        this.#side = side;
    }

    getArea = function(){
        return  this.#side*this.#side ; 
    }
}

let sq = new Square();
sq.setSide(30)
console.log(sq.getArea())
```

>Output
```
900
```

After creating the class Square, we create an Object sq1.
Users can input the side and call the function `getArea()`. The user can not change the variable `side` of the object directly and has to call the `setSide()` function to set the dimensions of the object **sq1**.<br>
Moreover, all the variables and methods are encapsulated in the **class Square**.
This is Encapsulation.
<br>
<br>


## 2) Abstraction
```javascript
class Employee{
    #bonus
    #salary

    constructor(name){
        this.name = name
        this.#bonus = 1000
    }

    getBonus(){
        console.log(this.#bonus)
    }

    setBaseSalary(salary){
        this.#salary = salary
    }

    getBaseSalary(){
        console.log(`Base salary of employee ${this.name} is ${this.#salary}`)
    }

    getTotalSalary(){
        console.log(this.#salary + this.#bonus)
    }

}

let emp1 = new Employee('Rahul')
emp1.getBonus()
emp1.setBaseSalary(1000)
emp1.getBaseSalary()
emp1.getTotalSalary()
```
>Output
```
1000
Base salary of employee Rahul is 1000
2000
```
In the above code, an employee can calculate his total salary, and the implementation details are not exposed to him.
<br>
<br>

## 3) Inheritance

```javascript
class Person{
    
    constructor(_name,_age){
        this.name = _name;
        this.age = _age;
    }

    describePerson(){
        console.log(`My name is ${this.name} and my age is ${this.age}`);
    }
}


// Programmer class extends Person class and has access to all properties and methods of Person Class
class Programmer extends Person{

    constructor(_name, _age, _yearsOfExperience){
        super(_name, _age); // This calls the constructor of the parent class for _name and _age

        // Custom Behaviour of programmer class
        this.yearsOfExperience = _yearsOfExperience
    }

    // The following code function is only available for programmer and not person
    code(){
        console.log(`${this.name} is ${this.age} years old and has been coding for past ${this.yearsOfExperience} years!!`)
    }
}
let person1 = new Person('Shubham',18)
let programmer1 = new Programmer('Anupam',24,3)

person1.describePerson()

programmer1.describePerson()
programmer1.code()
```
>Output
```
My name is Shubham and my age is 18
My name is Anupam and my age is 24
Anupam is 24 years old and has been coding for past 3 years!!
```
In the above code, `person1` is an object of `class Person` and `programmer1` is an object of `class Programmer`.<br>
Programmer class is the child class of Person class and inherits all of its properties.<br>
Also, the Programmer class has some additional methods which can only be used for its object.
<br>
<br>

## 4) Polymorphism
```javascript
class Car{

    description(){
        console.log('Cars run on diesel or petrol or electricity')
    }

}

class DieselCar extends Car {

    description(){
        console.log('Diesel cars use diesel as fuel.')
    }
}

class ElectricCar extends Car {

    description(){
        console.log('Electric cars run on electricity')
    }
}

let car1 = new Car()
car1.description()

let creta = new DieselCar()
creta.description()

let tesla = new ElectricCar()
tesla.description()
```
>Output
```
Cars run on diesel or petrol or electricity
Diesel cars use diesel as fuel.
Electric cars run on electricity
```

In the above code, we can observe that `description()` method in the child class is getting overloaded and is changing its form depending on which class object is formed.

<br>

## <u>References<u>
- [More on ES6 classes](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes)
- [Object Oriented JavaScript](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object-oriented_JS)
- [JavaScript Classes](https://www.youtube.com/watch?v=2ZphE5HcQPQ&t=2717s)

